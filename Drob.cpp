#include "Drob.h"
#include <iostream>
#include <string>
using namespace std;

Drob operator+ (Drob &ax, Drob &bx)
{
	if (ax.getZn()<bx.getZn() || ax.getZn() > bx.getZn())
	{
		return Drob(ax.getCh() * bx.getZn() + bx.getCh() * ax.getZn(), ax.getZn()* bx.getZn());
	}
	
}

Drob Drob::operator--()
{
	Chisl--;
	Znamenatel--;
	return *this;
}

Drob Drob::operator--(int notused)
{
	Drob temp = *this;
	Chisl--;
	Znamenatel--;
	return temp;
}

Drob operator*(Drob ax, Drob bx)
{
	Drob temp;
	temp.Chisl = ax.Chisl * bx.Chisl;
	temp.Znamenatel = ax.Znamenatel * bx.Znamenatel;
	return temp;
}

bool operator==(Drob ax, Drob bx)
{
	if (ax.Chisl == bx.Chisl && ax.Znamenatel == bx.Znamenatel) return(ax.Chisl == bx.Chisl && ax.Znamenatel == bx.Znamenatel);
}

bool operator!=(Drob ax, Drob bx)
{
	if (ax.Chisl != bx.Chisl && ax.Znamenatel != bx.Znamenatel) return(ax.Chisl != bx.Chisl && ax.Znamenatel != bx.Znamenatel);
}

Drob Drob::operator=(Drob ax)
{
	Chisl = ax.Chisl;
	Znamenatel = ax.Znamenatel;
	return Drob(Chisl, Znamenatel);
}

void Drob::show()
{
	cout << Chisl<< "/" << Znamenatel<<"\n";
}

Drob::~Drob()

{
	
}
