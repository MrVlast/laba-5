#include "Drob.h"
#include <iostream>

using namespace std;
int main()
{
    Drob a(2, 5), b(3, 2), c;
	Drob a1(2, 5), b1(2, 5), c1(6, 8);
	Drob a3(2, 5), b3(3, 2), c3;
	Drob a2(1, 3), b2(2, 3), c2;
	
	cout << "c=a+b\n";
	c = a + b;
	cout << "\nc="; c.show(); cout << "a = "; a.show(); cout << "b = "; b.show();
	
	cout << "c=a*b\n";
	c2 = a2 * b2;
	cout << "\nc="; c2.show(); cout << "a = "; a2.show(); cout << "b = "; b2.show();

	cout << "a=b--";
	a3 = b3--;
	cout << "\na = "; a3.show(); cout << "b = "; b3.show();

	cout << "c=--b";
	c3 = --b2;
	cout << "\nc="; c3.show(); cout << "b = "; b2.show();

	cout << "a=b?";
	if (a1 == b1) cout <<  "- same\n";
	cout << "c=b?";
	if (c1 != b1) cout << "- Not the same\n";

	cout << "c=a";
	c = a;
	cout << "\nc="; c.show(); cout << "a = "; a.show();

}
