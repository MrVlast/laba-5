#ifndef DROB_H
#define DROB_H

class Drob
{
	double Chisl;
	double Znamenatel;
public:
	Drob() { Chisl = 1; Znamenatel = 2; }
	Drob(double z, double x) { Znamenatel = x; Chisl = z; }
	~Drob();
	void setCh(double c);
	void setZn(double v);
	double getCh() { return Chisl; }
	double getZn() { return Znamenatel; }
	Drob operator= (Drob ax);
	Drob operator--();
	Drob operator-- (int notused);
	friend Drob operator* (Drob ax, Drob bx);
	friend bool operator== (Drob ax, Drob bx);
	friend bool operator!= (Drob ax, Drob bx);
	void show();
};

Drob operator+ (Drob &ax1,Drob &bx);

#endif // !DROB_H
